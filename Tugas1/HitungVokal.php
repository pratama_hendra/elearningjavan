
<!DOCTYPE html>
<html>
<head>
    <title>Huruf Vokal</title>
</head>
<body>
<h1>Huruf Vocal</h1>

<form method="POST" action="<?php ?>">
    <p>
        Kata: <br>
        <input type="text" name="kata">
    </p>
    <p>
        <button type="submit">Cari</button>
    </p>
</form>
<?php
function hitung_huruf($kata){
    $arr = str_split($kata);
    $vocal = ['a', 'i', 'u', 'e', 'o'];
    $found = array_intersect($vocal, $arr);
    $count = array_count_values($arr);
    $result = [];
    foreach ($found as $item) {
        $result[$item] = $count[$item];
    }
    return $result;
}
?>

<?php if ($_POST && isset($_POST['kata'])): ?>
    <hr>
    Memiliki huruf vocal :
    <table ="1">
    <?php foreach (hitung_huruf($_POST['kata']) as $huruf => $vocal): ?>
        <tr>
            <td style="width:100px">Huruf  :<?php echo $huruf ?></td>
            <td style="width:100px">Jumlah :<?php echo $vocal ?></td>
        </tr>
    <?php endforeach ?>
        <?php foreach (hitung_huruf($_POST['kata']) as $huruf => $jumlah): ?>
            <tr>
                <td style="width:100px">Huruf  :<?php echo $huruf ?></td>
                <td style="width:100px">Jumlah :<?php echo $jumlah ?></td>
            </tr>
        <?php endforeach ?>
    </table>
<?php endif ?>
</body>
</html>
